"use strict";

(function(){
    if(navigator.userAgent === 'jsdom'){ return; }

    require('fastclick')(document.body);
    
    var state = require('state');

    var stuck = document.querySelector('html');
    var stuckHeight = 0;

    var component = document.querySelector('.componentMenu');
    var componentWidth = document.querySelector('.window--left');

    var scrollLinks = document.querySelectorAll('.anchor');
    var scrollTargets = document.querySelectorAll('.component');
    var scrollOffsets = [];
    var scrollFound = 0;

    var scrollLinksRoot = document.querySelectorAll('.anchor__root');
    var scrollTargetsRoot = document.querySelectorAll('.component__root');
    var scrollOffsetsRoot = [];
    var scrollFoundRoot = 0;

    if(/iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream){
        document.querySelector('html').classList.add('iOS');
    }

    component.style.width = componentWidth.offsetWidth + 'px';

    initToggle('Help');
    initToggle('View');

    window.onload = function(){
        document.querySelector('html').classList.add('loaded');

        update();
    };

    window.onresize = function(e){
        update();
    };

    window.onscroll = function(e) {
        findAnchor();
    };

    function update(){
        component.style.width = componentWidth.offsetWidth + 'px';
        stuckHeight = document.querySelector('header').offsetHeight;

        scrollOffsets = [];
        for(var i = 0; i < scrollTargets.length; i++){
            scrollOffsets.push(scrollTargets[i].offsetTop);
        }

        scrollOffsetsRoot = [];
        for(i = 0; i < scrollTargetsRoot.length; i++){
            scrollOffsetsRoot.push(scrollTargetsRoot[i].offsetTop);
        }

        findAnchor();
    }

    function initToggle(name){
        document.querySelector('#toggle' + name).onclick = function(){
            this.classList.toggle('active');
            document.querySelector('html').classList.toggle('full' + name);
            state.toggle('full' + name);

            update();
        };

        if(state.get('full' + name)){
            document.querySelector('html').classList.remove('full' + name);
            document.querySelector('#toggle' + name).classList.add('active');
        }
    }

    function findAnchor(){
        var offset = window.pageYOffset;

        if(offset > stuckHeight){
            stuck.classList.add('stuck');
        } else {
            stuck.classList.remove('stuck');
        }

        for(var i = scrollOffsets.length - 1; i >= 0; i--){
            
            // Plus 10 to stop some rounding issues and having it be the exact top of the screen is a bit annoying
            if((offset + 10) >= scrollOffsets[i]){
                if(scrollFound !== i){
                    scrollLinks[scrollFound].classList.remove('active');
                    scrollFound = i;
                    scrollLinks[scrollFound].classList.add('active');
                }
                break;
            }
        }

        for(i = scrollOffsetsRoot.length - 1; i >= 0; i--){
            // Plus 10 to stop some rounding issues and having it be the exact top of the screen is a bit annoying
            if((offset + 10) >= scrollOffsetsRoot[i]){
                if(scrollFoundRoot !== i){
                    scrollLinksRoot[scrollFoundRoot].classList.remove('expand');
                    scrollFoundRoot = i;
                    scrollLinksRoot[scrollFoundRoot].classList.add('expand');
                }
                break;
            }
        }
    }
})();