var ls = window.localStorage;

module.exports = exports = {
    get: function(name){
        return JSON.parse(ls.getItem(name));
    },
    set: function(name, value){
        ls.setItem(name, value);
    },
    toggle: function(name){
        var flip = !this.get(name);
        ls.setItem(name, flip);
        return flip;
    },
    reset: function(){
        ls.clear();
    }
};